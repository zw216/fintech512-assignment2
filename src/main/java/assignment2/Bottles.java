package assignment2;

class Bottles {

	public String verse(int verseNumber) {
		int verseNumber2=verseNumber-1;
		String a="bottles";
		String b="bottles";
		String n="one";

		if(verseNumber==0){
			return "No more bottles of beer on the wall, no more bottles of beer.\n"
					+ "Go to the store and buy some more, 99 bottles of beer on the wall.\n";
		}
		if(verseNumber==1){
			a="bottle";
			n="it";
		}
		if(verseNumber2==0){
			return verseNumber + " " + a + " of beer on the wall, " + verseNumber + " " + a + " of beer.\n"
					+ "Take " + n + " down and pass it around, no more bottles of beer on the wall.\n";

		}
		if (verseNumber2 == 1) {
				b = "bottle";
		}
		return verseNumber + " " + a + " of beer on the wall, " + verseNumber + " " + a + " of beer.\n"
					+ "Take " + n + " down and pass it around, " + verseNumber2 + " " + b + " of beer on the wall.\n";
	}

	public String verse(int startVerseNumber, int endVerseNumber) {
		String rtn="";
		int number=startVerseNumber;
		while(number>endVerseNumber){
			rtn+=new Bottles().verse(number)+"\n";
			number--;
		}
		rtn=rtn+new Bottles().verse(endVerseNumber);
		return rtn;
	}

	public String song() {
		return new Bottles().verse(99,0);
	}
}
